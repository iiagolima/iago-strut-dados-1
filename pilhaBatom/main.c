#include <stdio.h>
#include "batom.h"

int main(){
  Pilha batons;

  inicializarPilha(&batons);
  insere(&batons, "avon", "preto", 35);
  insere(&batons, "marykay", "vermelho", 40);
  insere(&batons, "jequiti", "amarelo", 45);
  removeBatom(&batons);
  exibeDados(&batons);

  return 0;
}
