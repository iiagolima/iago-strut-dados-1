#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTIDADE 5

typedef struct Batom{
  char marca[20];
  char cor[20];
  float preco;
}Batom;

typedef struct Pilha{
  Batom pack[QUANTIDADE];
  int topo;
}Pilha;

void inicializarPilha(Pilha *p){
  p->topo = -1;
};

void insere(Pilha *p, char marca[], char cor[], float preco){
  printf("-------- Cadastrando um novo Batom --------\n");

  Batom newBatom;
  strcpy(newBatom.marca, marca);
  strcpy(newBatom.cor, cor);
  newBatom.preco = preco;
  (*p).topo = p->topo+1;
  (*p).pack[p->topo] = newBatom;
  printf("-------- Batom %i cadastrado com Sucesso --------\n", p->topo+1);
}

void removeBatom(Pilha *p){
  printf("-------- Removendo último Batom inserido --------\n");
  printf("-------- Batom %i removido com Sucesso --------\n", p->topo+1);
  (*p).topo--;
}

void exibeDados(Pilha *p){
  printf("\n\n-------- Dados da Pilha --------\n");
    printf("Batom do topo da Pilha:\n");
    printf("Marca do Batom: %s\n", p->pack[p->topo].marca);
    printf("Cor do Batom: %s\n", p->pack[p->topo].cor);
    printf("Preço do Batom: %f\n", p->pack[p->topo].preco);
}
