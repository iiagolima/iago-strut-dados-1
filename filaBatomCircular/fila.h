#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTIDADE 3

typedef struct Batom{
  char marca[15];
  char cor[15];
  float preco;
}Batom;

typedef struct Fila{
  Batom fila[QUANTIDADE];
  int inicio, fim, qtdelemento;
}Fila;

void criar(Fila *f){
  printf("Criando Fila!\n");
  f->inicio = 0;
  f->qtdelemento = 0;
  f->fim = -1;
}

void inserir(Fila *f, char marca[], char cor[], float preco){
  if(verifica(f)){
    printf("\nFila está cheia!!\n");
    return;
  }

  if(f->fim == (QUANTIDADE - 1)){
    f->fim = -1;
  }

  printf("Inserindo ao fim da fila!\n");
  Batom newBatom;
  strcpy(newBatom.marca, marca);
  strcpy(newBatom.cor, cor);
  newBatom.preco = preco;
  f->fim++;
  f->fila[f->fim] = newBatom;
  f->qtdelemento++;
  printf("Inserido com sucesso!!\n");
}

void remover(Fila *f){
  if(f->qtdelemento == 0){
    printf("\nFila está vazia!!\n");
    return;
  }

  if(f->inicio == QUANTIDADE){
    f->inicio = 0;
  }

  printf("\nRemovendo 1º da fila.....\n");
  f->inicio++;
  f->qtdelemento--;
  printf("Removido com sucesso!\n");
}

void imprimirInicio(Fila *f){
  if(f->qtdelemento == 0){
    printf("\nFila está vazia!!\n");
  }else{
    printf("\nImprimindo 1º da Fila\n");
    printf("Marca do Batom: %s\n", f->fila[f->inicio].marca);
    printf("Cor do Batom: %s\n", f->fila[f->inicio].cor);
    printf("Preço do Batom: %f\n", f->fila[f->inicio].preco);
  }
}

int verifica(Fila *f){
  return (f->qtdelemento == QUANTIDADE);
}
