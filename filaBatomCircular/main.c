#include <stdio.h>
#include "fila.h"

int main() {
  Fila fila;

  criar(&fila);
  inserir(&fila, "Avon", "Vermelho", 20);
  inserir(&fila, "MaryKay", "Preto",30);
  inserir(&fila, "Jequiti", "Rosa", 35);
  imprimirInicio(&fila);
  remover(&fila);
  imprimirInicio(&fila);

  return 0;
}
