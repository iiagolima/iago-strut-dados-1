#define QUANTIDADE 5
#include <string.h>

typedef struct Batom{
  char marca[15];
  char cor[15];
  float preco;
}Batom;

typedef struct No{
  Batom batom;
  struct No *prox;
}No;

typedef struct No Lista;

Lista* criarLista(Lista *l){
  return NULL;
}

Lista* inserir_inicio(Lista *l, char marca[], char cor[], float preco){
  Lista* newBatom = (Lista *) malloc(sizeof(Lista));
  strcpy(newBatom->batom.marca, marca);
  strcpy(newBatom->batom.cor, cor);
  newBatom->batom.preco = preco;
  newBatom->prox = l;

  return newBatom;
}

void imprime(Lista *l){
  if(l == NULL){
    printf("Lista Vazia!");
  }

  Lista *aux;
  aux = l;
  while(aux != NULL){
    printf("Marca: %s\n", aux->batom.marca);
    printf("Cor: %s\n", aux->batom.cor);
    printf("Preço: %f\n\n", aux->batom.preco);
    aux = aux->prox;
  }
}

Lista* remover_inicio(Lista *l){
  if(l == NULL){
    printf("Lista já está vazia!");
    return NULL;
  }else{
    Lista *atual = l;
    l = atual->prox;
    return l;
  }
}

Lista* remover_especifico(Lista *l, float preco){
  if(l == NULL){
    printf("Lista já está vazia!");
  }else{
    Lista *anterior = NULL;
    Lista *atual = l;

    while(atual != NULL && atual->batom.preco != preco){
        anterior = atual;
        atual = atual->prox;
    }

    if(atual == NULL){
      printf("Preço não encontrado!");
      return l;
    }

    if(anterior == NULL){
      l = anterior->prox;
    }else{
      anterior->prox = atual->prox;
    }

    return l;
  }
}
