#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"

int main() {
  Lista *lista;

  lista = criarLista(lista);

  lista = inserir_inicio(lista, 10);
  lista = inserir_inicio(lista, 20);
  lista = inserir_inicio(lista, 30);

  printf("Imprime:\n");
  imprime(lista);

  printf("\nImprime Invetido:\n");
  imprimeInvertido(lista);

  busca(lista, 40);

  remover_especifico(lista, 10);

  printf("\nImprime:\n");
  imprime(lista);

  return 0;
}
