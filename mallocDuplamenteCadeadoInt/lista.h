#define QUANTIDADE 50

typedef struct No{
  int val;
  struct No *prox;
  struct No *ant;
}No;

typedef struct No Lista;

Lista* criarLista(Lista *l){
  return NULL;
}

Lista* inserir_inicio(Lista *l, int val){
  Lista* newNo = (Lista *) malloc(sizeof(Lista));
  if(l != NULL){
    l->ant = newNo;
  }
  newNo->val = val;
  newNo->prox = l;
  newNo->ant = NULL;
  return newNo;
}

void imprime(Lista *l){
  if(l == NULL){
    printf("Lista Vazia!\n");
  }
  Lista *aux;
  aux = l;
  while(aux != NULL){
    printf("%d\n", aux->val);
    aux = aux->prox;
  }
}

void imprimeInvertido(Lista *l){
  if(l == NULL){
    return;
  }else{
    imprimeInvertido(l->prox);
    printf("%d\n",l->val);
  }
}

Lista* busca(Lista *l, int val){
  if(l == NULL){
    printf("Lista já está vazia!\n");
  }else{
    Lista *aux;
    aux = l;

    while(aux != NULL && aux->val != val){
        aux = aux->prox;
    }

    if(aux == NULL){
      printf("Valor não encontrado!\n");
      return l;
    }
    printf("\nValor Buscado: %d\n", aux->val);
  }
}

Lista* remover_inicio(Lista *l){
  if(l == NULL){
    printf("Lista já está vazia!\n");
    return NULL;
  }else{
    Lista *aux;
    aux = l;

    if(aux->ant == NULL){
      aux = aux->prox;
      aux->ant = NULL;
      free(aux);
    }

    return l;
  }
}

Lista* remover_especifico(Lista *l, int val){
  if(l == NULL){
    printf("Lista já está vazia!\n");
  }else{
    Lista *aux;
    aux = l;

    while(aux != NULL && aux->val != val){
        aux = aux->prox;
    }

    if(aux == NULL){
      printf("Valor não encontrado!\n");
      return l;
    }

    if(aux->ant == NULL){
      l = (aux->ant)->prox;
    }else{
      (aux->ant)->prox = aux->prox;
      free(aux);
    }

    return l;
  }
}
