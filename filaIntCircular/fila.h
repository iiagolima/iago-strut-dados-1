#include <stdio.h>
#include <stdlib.h>
#define QUANTIDADE 3

// typedef struct Camisa{
//   char tamanho[15];
//   char cor[15];
//   float preco;
// }Camisa;

typedef struct Fila{
  int fila[QUANTIDADE];
  int inicio, fim, qtdelemento;
}Fila;

//Cria Fila com valores iniciais
void criar(Fila *f){
  printf("Criando Fila!\n");
  f->inicio = 0;
  f->qtdelemento = 0;
  f->fim = -1;
}

//Insere elemento na Fila
void inserir(Fila *f, int val){
  //Verifica se a fila está cheia
  if(verifica(f)){//Caso cheia, imprime msg
    printf("\nFila está cheia!!\n");
    return;
  }

  if(f->fim == (QUANTIDADE - 1)){//Verifica se fim é igual a QUANTIDADE menos 1
    f->fim = -1;//Caso igual, joga o fim da fila para o ponto inicial
  }

  printf("Inserindo %d ao fim da fila!\n", val);
  f->fim++;//Incrementa FIM
  f->fila[f->fim] = val;//Insere elemento no FIM
  f->qtdelemento++;//Mais um item inserido
  printf("Inserido com sucesso!!\n");
}

void remover(Fila *f){
  if(f->qtdelemento == 0){//Veriif(){fica se a fila está vazia
    printf("\nFila está vazia!!\n");
    return;
  }

  if(f->inicio == QUANTIDADE){//Verifica se o inicio é igual a QUANTIDADE
    f->inicio = 0;//Caso seja, reseta o inicio para o valor inicial(0);
  }

  printf("Removendo 1º da fila.....\n");
  f->inicio++;//Incrementa início em 1;
  f->qtdelemento--;//Diminui a quantidade de elementos em 1
  printf("Removido com sucesso!\n");
}

void imprimirInicio(Fila *f){
  if(f->qtdelemento == 0){//Verifica se a fila está vazia
    printf("\nFila está vazia!!\n");
  }else{//Caso não esteja vazia;
    printf("\nImprimindo 1º da Fila --> %d\n\n", f->fila[f->inicio]);
  }
}

int verifica(Fila *f){
  //Verifica se a quantidade de elementos é igual ao máximo aceitável
  return (f->qtdelemento == QUANTIDADE);
}
