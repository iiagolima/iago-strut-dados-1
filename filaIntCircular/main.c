#include <stdio.h>
#include "fila.h"

int main() {
  Fila fila;

  criar(&fila);
  inserir(&fila, 10);
  inserir(&fila, 20);
  inserir(&fila, 30);
  inserir(&fila, 40);
  imprimirInicio(&fila);
  remover(&fila);
  imprimirInicio(&fila);

  return 0;
}
