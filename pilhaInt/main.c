#include <stdio.h>
#include "pilha.h"

int main(){
  Pilha inteiros;

  inicializarPilha(&inteiros);
  insere(&inteiros, 35);
  insere(&inteiros, 40);
  insere(&inteiros, 45);
  removeInteiro(&inteiros);
  exibeDados(&inteiros);

  return 0;
}
