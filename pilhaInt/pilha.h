#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTIDADE 5


typedef struct Pilha{
  int pack[QUANTIDADE];
  int topo;
}Pilha;

void inicializarPilha(Pilha *p){
  p->topo = -1;
};

void insere(Pilha *p, int val){
  printf("-------- Cadastrando um novo Inteiro --------\n");
  (*p).topo = p->topo+1;
  (*p).pack[p->topo] = val;
  printf("-------- Inteiro %i cadastrado com Sucesso --------\n", p->topo+1);
}

void removeInteiro(Pilha *p){
  printf("-------- Removendo último Inteiro inserido --------\n");
  printf("-------- Inteiro %i removido com Sucesso --------\n", p->topo+1);
  (*p).topo--;
}

void exibeDados(Pilha *p){
  printf("\n\n-------- Dados da Pilha --------\n");
    printf("Inteiro do topo da Pilha:\n");
    printf("Inteiro: %d\n", p->pack[p->topo]);
}
