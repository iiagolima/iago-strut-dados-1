#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"

#define QTD 50

int main() {
  Lista *lista;

  lista = criarLista(lista);
  
  lista = inserir_inicio(lista, 10);
  lista = inserir_inicio(lista, 20);
  lista = inserir_inicio(lista, 30);

  imprime(lista);

  lista = remover_especifico(lista, 10);

  imprime(lista);

  return 0;
}
