#define QUANTIDADE 50

typedef struct No{
  int val;
  struct No *prox;
}No;

typedef struct No Lista;

Lista* criarLista(Lista *l){
  return NULL;
}

Lista* inserir_inicio(Lista *l, int val){
  Lista* newNo = (Lista *) malloc(sizeof(Lista));
  newNo->val = val;
  newNo->prox = l;

  return newNo;
}

void imprime(Lista *l){
  if(l == NULL){
    printf("Lista Vazia!");
  }

  Lista *aux;
  aux = l;
  while(aux != NULL){
    printf("%d\n", aux->val);
    aux = aux->prox;
  }
}

Lista* remover_inicio(Lista *l){
  if(l == NULL){
    printf("Lista já está vazia!");
    return NULL;
  }else{
    Lista *atual = l;
    l = atual->prox;
    return l;
  }
}

Lista* remover_especifico(Lista *l, int val){
  if(l == NULL){
    printf("Lista já está vazia!");
  }else{
    Lista *anterior = NULL;
    Lista *atual = l;

    while(atual != NULL && atual->val != val){
        anterior = atual;
        atual = atual->prox;
    }

    if(atual == NULL){
      printf("Valor não encontrado!");
      return l;
    }

    if(anterior == NULL){
      l = anterior->prox;
    }else{
      anterior->prox = atual->prox;
    }

    return l;
  }
}
