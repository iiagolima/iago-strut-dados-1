#include <stdio.h>
#include "fila.h"

int main() {
  Fila fila;

  criar(&fila);
  inserir(&fila, 10);
  inserir(&fila, 20);
  inserir(&fila, 30);
  inserir(&fila, 40);
  inserir(&fila, 50);
  inserir(&fila, 60);
  inserir(&fila, 70);
  inserir(&fila, 80);
  inserir(&fila, 90);
  inserir(&fila, 100);
  imprimirInicio(&fila);
  remover(&fila);
  imprimirInicio(&fila);

  return 0;
}
