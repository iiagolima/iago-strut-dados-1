#include <stdio.h>
#include <stdlib.h>
#define QUANTIDADE 10

// typedef struct Camisa{
//   char tamanho[15];
//   char cor[15];
//   float preco;
// }Camisa;

typedef struct Fila{
  int fila[QUANTIDADE];
  int inicio, fim;
}Fila;

void criar(Fila *f){
  printf("Criando Fila!\n");
  f->inicio = 0;
  f->fim = -1;

}

void inserir(Fila *f, int val){
  // if(nao cheia){
  printf("Inserindo %d ao fim da fila!\n", val);
  f->fim++;
  f->fila[f->fim] = val;
  // printf("Inserido com sucesso!!\n");
  // }
}

void remover(Fila *f){
  // if(vazia){
  //  printf("A fila está vazia!");
  // }else{
  printf("Removendo 1º da fila.....\n");
  f->inicio++;
  printf("Removido com sucesso!\n");
  // }
}

void imprimirInicio(Fila *f){
  printf("\nImprimindo 1º da Fila --> %d\n\n", f->fila[f->inicio]);
}
