#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QUANTIDADE 5

typedef struct Batom{
  char marca[15];
  char cor[15];
  float preco;
}Batom;

typedef struct Fila{
  Batom fila[QUANTIDADE];
  int inicio, fim;
}Fila;

void criar(Fila *f){
  printf("Criando Fila!\n");
  f->inicio = 0;
  f->fim = -1;
}

void inserir(Fila *f, char marca[], char cor[], float preco){
  printf("Inserindo ao fim da fila!\n");
  Batom newBatom;
  strcpy(newBatom.marca, marca);
  strcpy(newBatom.cor, cor);
  newBatom.preco = preco;
  f->fim++;
  f->fila[f->fim] = newBatom;
}

void remover(Fila *f){
  printf("\n\nRemovendo 1º da fila.....\n");
  f->inicio++;
  printf("Removido com sucesso!\n");
}

void imprimirInicio(Fila *f){
  printf("\nImprimindo 1º da Fila\n");
  printf("Marca do Batom: %s\n", f->fila[f->inicio].marca);
  printf("Cor do Batom: %s\n", f->fila[f->inicio].cor);
  printf("Preço do Batom: %f\n", f->fila[f->inicio].preco);
}
